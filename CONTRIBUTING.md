<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Acura Multi purpose Free HTML5 Template</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://webthemez.com" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/fancybox/jquery.fancybox.css" rel="stylesheet"> 
<link href="css/flexslider.css" rel="stylesheet" /> 
<link href="css/style.css" rel="stylesheet" />

</head>
<body>
<div id="wrapper" class="home-page">
<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12"> 	  
        <p class="pull-left hidden-xs"><i class="fa fa-clock-o"></i><span>Tutorial de Gitlab</span></p>
        <p class="pull-right"><i class="fa fa-letter"></i>Atento a tus dudas con el tema en los comentarios</p>
      </div>
    </div>
  </div>
</div>
	<!-- start header -->
	<header>
		
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="img/logo.jpg"alt="logo"width="300px" height="50px"/></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.html">Inicio</a></li> 
						 <li class="dropdown">
                       
                    
                        <li><a href="login1.html">Historia</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
	<section id="banner">
	 
	<!-- Slider -->
        <div id="main-slider" class="flexslider">
            <ul class="slides">
              <li>
                <img src="img/slides/3.png" alt="" width="550px" height="300px" />
                <div class="flex-caption"> 
                <h4>Aprendiendo algo sobre</h4 >
                </div>
              </li>
              <li>
                <img src="img/slides/4.png" alt="" width="550px" height="300px"/>
                <div class="flex-caption">
					<h4>La Mejor Plataforma de Git</h4 > 
					 
                </div>
              </li>
            </ul>
        </div>
	<!-- end slider -->
	</section> 
			<center>
			<h3><span>Informacion Sobre Gitlab</span></h3>
		</div>		
		<!-- Accordion starts -->
		<div class="panel-group" id="accordion-alt3">
		 <!-- Panel. Use "panel-XXX" class for different colors. Replace "XXX" with color. -->
		  <div class="panel">	
			<!-- Panel heading -->
			 <div class="panel-heading">
				<h4 class="panel-title">
				  <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseOne-alt3">
					<i class="fa fa-angle-right"></i>Crear cuenta 
				  </a>
				</h4>
			 </div>
			 <div id="collapseOne-alt3" class="panel-collapse collapse">
				<div class="panel-body">
				  Para que la solicitud sea procesada debes estar registrado en el sistema y llenar los campos requeridos consernientes a la solicitud 
				</div>
			 </div>
		  </div>
		  <div class="panel">
			 <div class="panel-heading">
				<h4 class="panel-title">
				  <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseTwo-alt3">
					<i class="fa fa-angle-right"></i>Configuracion de tu cuenta 
				  </a>
				</h4>
			 </div>
			 <div id="collapseTwo-alt3" class="panel-collapse collapse">
				<div class="panel-body">
				  Para procesar los requerimiento debes tener el usuario con los permisos de acceso al sistema 
				</div>
			 </div>
		  </div>
		  <div class="panel">
			 <div class="panel-heading">
				<h4 class="panel-title">
				  <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseThree-alt3">
					<i class="fa fa-angle-right"></i>Crear proyecto
				  </a>
				</h4>
			 </div>
			 <div id="collapseThree-alt3" class="panel-collapse collapse">
				<div class="panel-body">
				  Para Asignar el tecnico debes tener una solicitud disponible y ya 
				  haber identificado los requerimientos 
				</div>
			 </div>
		  </div>
		  <div class="panel">
			 <div class="panel-heading">
				<h4 class="panel-title">
				  <a data-toggle="collapse" data-parent="#accordion-alt3" href="#collapseThree-alt3">
					<i class="fa fa-angle-right"></i>Realizar commit
				  </a>
				</h4>
			 </div>
			 <div id="collapseThree-alt3" class="panel-collapse collapse">
				<div class="panel-body">
				  Para Asignar el tecnico debes tener una solicitud disponible y ya 
				  haber identificado los requerimientos 
				</div>
			 </div>
		  </div>
		<!-- Accordion ends -->
		</center>
	</div>						 
<br>

</div>

</div>
						
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>
							<span>&copy; Bootstrap Template 2017 All right reserved. Template By </span><a href="http://webthemez.com/free-bootstrap-templates/" target="_blank">WebThemez</a>
						</p>
					</div>
				</div>
				<div class="col-lg-6">
					<ul class="social-network">
						<li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>  
<script src="js/jquery.flexslider.js"></script>
<script src="js/animate.js"></script>
<!-- Vendor Scripts -->
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script> 
</body>
</html>